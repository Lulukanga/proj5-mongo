# Project 5: Brevet time calculator with Ajax and MongoDB

**Author**: Lindsey Uribe

**Contact**: luribe@uoregon.edu

## A controle-time calculator for your next brevet-venture!

**Basic Functionality**: 
A user will design their next brevet-venture by selecting from a drop down list of possible brevet distances, 
then choosing a starting date and time. Default times are set to today's date and time. 

Users will then begin choosing their ideal controle distances. Users have the option of entering distances in miles
or kilometers, whichever degree of freedom remains will automatically be calculated and displayed along with the 
corresponding opening and close times. Population of the conversions, opening and closing times for each controle are 
done dynamically using AJAX and JQuery. Users have the additional option of entering a location if they wish to do so.

When the user has entered a valid final controle point (final controle points are defined as being at least as far as, 
or 20% beyond the total brevet distance), they will then be allowed to hit the submit button. Any attempts to click the
submit button prior to entry of a valid final control point will cause an error page to be flashed to the user. Upon 
clicking submit, data is stored in a database. Multiple brevets can be entered in this way, each one being stored into 
the database. 

Upon clicking the `display` button at the bottom of the page, the user's previously entered data for each brevet will be
retrieved and displayed on a new page.

**Error Cases**:  
There are numerous possible ways a user could encounter an error after hitting the **submit button**: 

 - The user attempts to hit the submit button when no controles have been entered
	- In this case, an error message is flashed at the top of the screen. 
   
 - The user attempts to submit with a final controle point that is more than 20% beyond the total distance of the brevet.
	- In this instance, the entered text is automatically removed, and an error message is flashed at the top of the screen to prevent inappropriate distances from being entered.    

 - The user attempts to submit an invalid controle point (no negative controle points allowed)
	- The entered text is automatically removed, and an error message is flashed at the top of the screen to prevent inappropriate distances from being entered.
   
There is one possible way a user could encounter an error after hitting the **display button**: 

 - The user attempts to hit the display button when no controles have yet been submitted to the database
	- In this case, the user is directed to a page in which an error message informs them as to the empty status of the database. A convenient "Go Back" button is located on this page is well, just in case a user is unaware of the built-in back button in their browser. 
   
