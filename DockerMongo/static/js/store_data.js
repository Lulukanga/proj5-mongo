function storeData() {
//    var json_data = {
//                        distance: $("#brevet_dist_km").val(),
//                        begin_date: $("#begin_date").val(),
//                        begin_time: $("#begin_time").val(),
//                        controls: []
//    };
    var distance = $("#brevet_dist_km").val()
    console.log("here's the distance of the total brevet:", distance)
    var ok_store = false;
    var is_valid = false;
    $(".control").each( function() {
        var km = $(this).find("input[name='km[]']").val();
        console.log("heres km: ", km);
        if (Number(km) > 0 ) {
            is_valid = true;
        }
        if (Number(km) >= Number(distance)) {
            console.log("Valid distance received. Ok to store now")
            ok_store = true;
        }
        })
//        if (Number(km) > 0) {
//            var miles = $(this).find("input[name='miles']").val();
//            var location = $(this).find("input[name='location']").val();
//            var open = $(this).find("input[name='open']").val();
//            var close = $(this).find("input[name='close']").val();
//            var notes = $(this).find(".notes").text();
//            json_data.controls.push({
//                km: km,
//                miles: miles,
//                location: location,
//                open: open,
//                close: close,
//                notes: notes
//            });
//        } //end of each
//    }) //end of if statement
    console.log("Someday I might store this: ", $('form').serialize());
//    console.log("Someday I might store this: " + JSON.stringify(json_data.controls[1]));
    if (ok_store) {
        $("#notifications").html("Successfully stored plans for your next brevet-venture!").fadeIn().fadeOut(3000);
        $.post('/new',  $('form').serialize());
    } else {
        $("#errors").html("Final controle distance must be at least as far as total brevet distance").fadeIn().fadeOut(3000);
        if (!is_valid) {
            $("#errors").html("Invalid Submission: Cannot store empty dataset").fadeIn().fadeOut(3000);
        }
    }
    }