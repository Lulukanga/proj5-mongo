"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
# import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB, and name the database, all in one
db = client.brevet_calc


@app.route('/todo')
def todo():
    _items = db.brevet_calc.find()
    items = [item for item in _items]
    app.logger.info(items)
    app.logger.info(len(items))
    return render_template('todo.html', items=items)


# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/new', methods=['POST'])
def new():
    data_list = []
    miles_array = request.form.getlist('miles[]')
    km_array = request.form.getlist('km[]')
    loc_array = request.form.getlist('location[]')
    open_array = request.form.getlist('open[]')
    close_array = request.form.getlist('close[]')
    for i in range(len(miles_array)):
        if len(miles_array[i]) != 0:
            data_list.append({"miles": miles_array[i],
                              "km": km_array[i],
                              "location": loc_array[i],
                              "open": open_array[i],
                              "close": close_array[i]
                              })

    item_doc = {
        'distance': request.form['distance'],
        'begin_date': request.form['begin_date'],
        'begin_time': request.form['begin_time'],
        'control_data': data_list
    }
    db.brevet_calc.insert_one(item_doc)
    return redirect(url_for('index'))


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.info("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    control_dist_km = request.args.get('km', 999, type=float)  # control kms
    brevet_dist = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    # app.logger.info(f"km={control_dist_km}, brevet_dist={brevet_dist}, date={begin_date}, time={begin_time}")
    # app.logger.info(f"request.args: {request.args}")
    race_start = f"{begin_date} {begin_time}"
    try:
        open_time = acp_times.open_time(control_dist_km, brevet_dist, race_start)
    except Exception as e:
        app.logger.exception(e)
        # error_msg = "Control point is too far beyond brevet distance!\nPlease try a shorter distance."
        error_msg = str(e)
        result = {"error_msg": error_msg}
    else:
        close_time = acp_times.close_time(control_dist_km, brevet_dist, race_start)
        is_final = acp_times.final_control(brevet_dist, control_dist_km)
        result = {"open": open_time, "close": close_time, "final": is_final}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    # print(f"Opening for global access on port {CONFIG.PORT}")
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
